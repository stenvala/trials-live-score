# importing the requests library 
import common
import requests 

# login
userName = "stenvala"
password = "testipesti"
sessionId = common.login(userName, password)

URL = common.BASE_URL + "user/me"

cookies = {"SESSION_ID": sessionId}

response = requests.get(url = URL, cookies=cookies)   

json = response.json()

group_id = ""
for i in json["myGroups"]:
	if i["name"] == "Testi pesti":
		group_id = i["id"]
		break

URL = common.BASE_URL + "group/" + group_id
response = requests.get(url = URL, cookies=cookies)   

print(response.content)

print "Get members with session id";
URL = common.BASE_URL + "group/" + group_id + "/members"
response = requests.get(url = URL, cookies=cookies)   

print(response.content)

# get with token
print "Solve JWT";

URL = common.BASE_URL + "user/me"

# common.verbose_requests(True)
response = requests.get(url = URL, cookies=cookies)   

jwtcookie = {"JWT_PERMISSIONS": response.cookies["JWT_PERMISSIONS"]};

print "Get members with JWT";

URL = common.BASE_URL + "group/" + group_id + "/members"
response = requests.get(url = URL, cookies=jwtcookie)   

print(response.content)

json = response.json()
member_id = ""
for i in json["results"]:
	if i["givenName"] == "Test":
		member_id = i["id"]


print "Get roles with JWT";

URL = common.BASE_URL + "group/" + group_id + "/roles"
response = requests.get(url = URL, cookies=jwtcookie)   

print(response.content)

json = response.json()
admin_role_id = ""
member_role_id = ""
for i in json["results"]:
	if i["type"] == "Admin":
		admin_role_id = i["id"]
	elif i["type"] == "Member":
		member_role_id = i["id"]


# Set to admin
URL = common.BASE_URL + "group/member/" + member_id + "/assign-role/" + admin_role_id
#response = requests.put(url = URL, cookies=jwtcookie)   
response = requests.put(url = URL, cookies=jwtcookie)   

print (response.content);

# Set to member
URL = common.BASE_URL + "group/member/" + member_id + "/assign-role/" + member_role_id
response = requests.put(url = URL, cookies=jwtcookie)   

print (response.content);


