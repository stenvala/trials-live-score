import requests
import logging

BASE_URL = "http://localhost:5002/api/"

def login(userName, password):	

	URL = BASE_URL + "user/login"
	response = requests.post(url = URL, json={"userName": userName, "password": password})   	
	json = response.json()

	return json["currentSession"]["id"]

def logout(sessionId):	

	URL = BASE_URL + "user/logout"
	response = requests.get(url = URL, cookies={"SESSION_ID": sessionId})   		

def logout_all(sessionId):	

	URL = BASE_URL + "user/logout-all"
	response = requests.get(url = URL, cookies={"SESSION_ID": sessionId})

def verbose_requests(ison):
	
	# These two lines enable debugging at httplib level (requests->urllib3->http.client)
	# You will see the REQUEST, including HEADERS and DATA, and RESPONSE with HEADERS but without DATA.
	# The only thing missing will be the response.body which is not logged.
	try:
	    import http.client as http_client
	except ImportError:
	    # Python 2
	    import httplib as http_client
	http_client.HTTPConnection.debuglevel = 1

	# You must initialize logging, otherwise you'll not see debug output.
	logging.basicConfig()
	logging.getLogger().setLevel(logging.DEBUG)
	requests_log = logging.getLogger("requests.packages.urllib3")	
	requests_log.propagate = True

	if ison:
		requests_log.setLevel(logging.DEBUG)
	else:
		requests_log.setLevel(loggin.NONE)
