# importing the requests library 
import common

import requests 
  
# create roles
URL = common.BASE_URL + "init/create-roles"  
response = requests.get(url = URL)   
print(response.status_code)
print(response.content)

# create sysadmin
URL = common.BASE_URL + "init/create-sysadmin"  
response = requests.get(url = URL)   
print(response.status_code)
print(response.content)

# create other user
URL = common.BASE_URL + "init/create-helper"  
response = requests.get(url = URL)   
print(response.status_code)
print(response.content)