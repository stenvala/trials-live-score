# importing the requests library 
import common

import requests 

# login
userName = "stenvala"
password = "testipesti"
sessionId = common.login(userName, password)

# get my data
URL = common.BASE_URL + "user/me"

cookies = {"SESSION_ID": sessionId}

response = requests.get(url = URL, cookies=cookies)   
print(response.status_code)
print(response.content)

# logout
common.logout(sessionId)

# should fail
response = requests.get(url = URL, cookies=cookies)   
print(response.status_code)
print(response.content)