﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using Rest.Models.User;

namespace Rest.Models.Event
{
    [Table("EV_RACE")]
    public class Race
    {

        public enum RaceStage
        {
            Final,
            Semifinal,
            Quarterfinal,
            Preliminary
        }

        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public Guid Id { get; set; }

        public string Name { get; set; }

        public RaceStage Stage;

        public DateTime From { get; set; }

        public DateTime To { get; set; }

        [Required]
        public Guid CategoryId { get; set; }

        [ForeignKey(nameof(CategoryId))]
        public Category Category { get; set; }

        public virtual ICollection<Race> Races { get; set; }

    }


}
