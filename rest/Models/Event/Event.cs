﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using Rest.Models.User;

namespace Rest.Models.Event
{
    [Table("EV_EVENT")]
    public class Event
	{		

		[Key]
		[DatabaseGenerated(DatabaseGeneratedOption.Identity)]
		public Guid Id { get; set; }

        [Required]
		public string Name { get; set; }

        [Required]
        public DateTime From { get; set; }

        [Required]
        public DateTime To { get; set; }

        [Required]
        public string City { get; set; }

        public string DetailCity { get; set; }

        [Required]
        public string Country { get; set; }

        public decimal Latitude { get; set; }

        public decimal Longiture { get; set; }

        [Required]
        public Guid GroupId { get; set; }

        [ForeignKey(nameof(GroupId))]
        public Group Group { get; set; }

        public virtual ICollection<Category> Categories { get; set; }

    }


}
