﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Rest.Models.Event
{
    [Table("EV_CATEGORY")]
    public class Category
    {

        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public Guid Id { get; set; }

        public string Name { get; set; }        

        [Required]
        public Guid SharedCategoryId { get; set; }

        [ForeignKey(nameof(SharedCategoryId))]
        public virtual Shared.Category SharedCategory { get; set; }

        [Required]
        public Guid EventId { get; set; }

        [ForeignKey(nameof(EventId))]
        public virtual Event Event { get; set; }

        
    }


}
