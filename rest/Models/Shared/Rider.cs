﻿using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Rest.Models.Shared
{
    [Table("CMN_RIDER")]
    public class Rider
    {
        public Rider()
        {
        }

        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public Guid Id { get; set; }

        [Required]
        [StringLength(150, MinimumLength = 3)]
        public string GivenName { get; set; }

        [Required]
        [StringLength(150, MinimumLength = 3)]
        public string FamilyName { get; set; }

        [Required]
        [StringLength(150, MinimumLength = 3)]
        public int UCI_ID { get; set; }

    }
}
