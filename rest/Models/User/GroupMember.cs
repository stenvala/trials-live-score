﻿using System;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Rest.Models.User
{
    [Table("US_GROUP_MEMBER")]
    public class GroupMember
    {                 

        [Key]
        [Required]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public Guid Id { get; set; }

        [Required]        
        public bool IsDeleted { get; set; }

        [Required]
        public Guid GroupId { get; set; }
        [ForeignKey(nameof(GroupId))]
        public Group Group { get; set; }

        [Required]
        public Guid UserId { get; set; }        
        [ForeignKey(nameof(UserId))]
        public User User { get; set; }        

        [Required]
        public Guid RoleId { get; set; }
        [ForeignKey(nameof(RoleId))]
        public GroupMemberRole Role { get; set; }        

    }
}