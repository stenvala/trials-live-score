﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using Newtonsoft.Json;
using Newtonsoft.Json.Converters;

namespace Rest.Models.User
{
    [JsonConverter(typeof(StringEnumConverter))]
    public enum GroupType
    {
        Normal,
        BackOffice
    }    

    [Table("US_GROUP")]
    public class Group
    {        

        [Key]        
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public Guid Id { get; set; }

        [Required]
        [StringLength(150, MinimumLength = 3)]
        public string Name { get; set; }

        [Required]
        public GroupType Type { get; set; }

        public Guid? ParentGroupId { get; set; }
        [ForeignKey(nameof(ParentGroupId))]
        public Group ParentGroup { get; set; }

        [InverseProperty(nameof(ParentGroup))]
        public virtual List<Group> ChildGroups { get; set; }

        public virtual ICollection<GroupMember> Members { get; set; }

    }


}
