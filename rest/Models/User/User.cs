﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Security.Cryptography;

namespace Rest.Models.User
{
    public class CreateableUser
    {
        public string UserName { get; set; }
        public string GivenName { get; set; }
        public string FamilyName { get; set; }
        public string Email { get; set; }
        public bool CanUseOtherUsers { get; set; }        
    }

    [Table("US_USER")]
    public class User
    {        

        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public Guid Id { get; set; }
    
        [StringLength(31, MinimumLength = 5)]        
        public string UserName { get; set; }

        [StringLength(31)]
        public string GivenName { get; set; }

        [StringLength(31)]
        public string FamilyName { get; set; }

        public string Email { get; set; }
        public string PasswordHash { get; set; }        

        public bool CanUseOtherUsers { get; set; }

        public virtual ICollection<GroupMember> GroupMemberships { get; set; }

        public virtual ICollection<Session> Sessions { get; set; }

        public User()
        {
            // this.GroupMemberships = new List<GroupMember>();
        }


        public static string HashPassword(string password)
        {
            // How to: https://stackoverflow.com/questions/4181198/how-to-hash-a-password/10402129#10402129
            byte[] salt;
            new RNGCryptoServiceProvider().GetBytes(salt = new byte[16]);

            var pbkdf2 = new Rfc2898DeriveBytes(password, salt, 1000);
            byte[] hash = pbkdf2.GetBytes(20);

            byte[] hashBytes = new byte[36];
            Array.Copy(salt, 0, hashBytes, 0, 16);
            Array.Copy(hash, 0, hashBytes, 16, 20);

            string savedPasswordHash = Convert.ToBase64String(hashBytes);
            return savedPasswordHash;
        }

        public Boolean IsPasswordValid(string password)
        {            
            string savedPasswordHash = PasswordHash;            
            byte[] hashBytes = Convert.FromBase64String(savedPasswordHash);         
            byte[] salt = new byte[16];
            Array.Copy(hashBytes, 0, salt, 0, 16);            
            var pbkdf2 = new Rfc2898DeriveBytes(password, salt, 1000);
            byte[] hash = pbkdf2.GetBytes(20);
            for (int i = 0; i < 20; i++)
                if (hashBytes[i + 16] != hash[i])
                    return false;
            return true;
        }        

    }



}