﻿using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Rest.Models.User
{
    public class GroupMemberPermission
    {
        public GroupMemberPermission()
        {
        }

        public enum MemberPermission
        {
            CreateEvent,
            GetEvent,
            UpdateEvent,
            DeleteEvent,
            EditSealedEvent,
            SealEvent,
            AddMember,
            EditMember,
            GetMember,
            DeleteMember,
            InviteReferee,
            RemoveReferee
        }

        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public Guid Id { get; set; }

        public MemberPermission Permission { get; set; }


    }
}
