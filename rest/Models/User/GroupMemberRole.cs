﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using Newtonsoft.Json;
using Newtonsoft.Json.Converters;

namespace Rest.Models.User
{
    [JsonConverter(typeof(StringEnumConverter))]
    public enum RoleType
    {
        Admin,
        Member,
        Custom
    }    


    [Table("US_GROUP_MEMBER_ROLE")]
    public class GroupMemberRole
    {        

        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public Guid Id { get; set; }

        [Required]
        public bool IsGlobal { get; set; }

        public string Name { get; set; }
        
        public RoleType Type { get; set; }

        public GroupType GroupType { get; set; }
        
        public virtual ICollection<GroupMemberRoleGroupMemberPermission> PermissionMap { get; set; }        

        public virtual ICollection<GroupMember> GroupMembers { get; set; }

        // If group's custom role
        public Guid? GroupId { get; set; }

        [ForeignKey(nameof(GroupId))]
        public virtual Group Group { get; set; }

    }

    [JsonConverter(typeof(StringEnumConverter))]
    public enum Permission
    {
        GetGroup,
        CreateEvent,
        GetEvent,
        UpdateEvent,
        DeleteEvent,
        EditSealedEvent,        
        SealEvent,
        AddMember,
        EditMember,
        GetMember,
        DeleteMember,
        InviteReferee,
        RemoveReferee,
        BackOfficeBrowseUsers,
        BackOfficeBrowseGroups
    }

    [Table("US_GROUP_MEMBER_PERMISSION")]
    public class GroupMemberPermission
    {        

        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public Guid Id { get; set; }
        
        public Permission Permission { get; set; }

        public virtual ICollection<GroupMemberRoleGroupMemberPermission> Roles { get; set; }

    }

    [Table("US_LINK_GROUP_MEMBER_ROLE_TO_PERMISSION")]
    public class GroupMemberRoleGroupMemberPermission
    {        

        [Required]
        public Guid GroupMemberRoleId { get; set; }
        [ForeignKey(nameof(GroupMemberRoleId))]
        public virtual GroupMemberRole GroupMemberRole { get; set; }

        [Required]
        public Guid GroupMemberPermissionId { get; set; }
        [ForeignKey(nameof(GroupMemberPermissionId))]
        public virtual GroupMemberPermission GroupMemberPermission { get; set; }
    }

}
