﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

namespace Rest.Migrations
{
    public partial class InitialCreate : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.CreateTable(
                name: "US_GROUP",
                columns: table => new
                {
                    Id = table.Column<Guid>(nullable: false),
                    Name = table.Column<string>(maxLength: 150, nullable: false),
                    Type = table.Column<string>(nullable: false),
                    ParentGroupId = table.Column<Guid>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_US_GROUP", x => x.Id);
                    table.ForeignKey(
                        name: "FK_US_GROUP_US_GROUP_ParentGroupId",
                        column: x => x.ParentGroupId,
                        principalTable: "US_GROUP",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateTable(
                name: "US_GROUP_MEMBER_PERMISSION",
                columns: table => new
                {
                    Id = table.Column<Guid>(nullable: false),
                    Permission = table.Column<string>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_US_GROUP_MEMBER_PERMISSION", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "US_USER",
                columns: table => new
                {
                    Id = table.Column<Guid>(nullable: false),
                    UserName = table.Column<string>(maxLength: 31, nullable: true),
                    GivenName = table.Column<string>(maxLength: 31, nullable: true),
                    FamilyName = table.Column<string>(maxLength: 31, nullable: true),
                    Email = table.Column<string>(nullable: true),
                    PasswordHash = table.Column<string>(nullable: true),
                    CanUseOtherUsers = table.Column<bool>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_US_USER", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "US_GROUP_MEMBER_ROLE",
                columns: table => new
                {
                    Id = table.Column<Guid>(nullable: false),
                    IsGlobal = table.Column<bool>(nullable: false),
                    Name = table.Column<string>(nullable: true),
                    Type = table.Column<string>(nullable: false),
                    GroupType = table.Column<string>(nullable: false),
                    GroupId = table.Column<Guid>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_US_GROUP_MEMBER_ROLE", x => x.Id);
                    table.ForeignKey(
                        name: "FK_US_GROUP_MEMBER_ROLE_US_GROUP_GroupId",
                        column: x => x.GroupId,
                        principalTable: "US_GROUP",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateTable(
                name: "US_SESSION",
                columns: table => new
                {
                    Id = table.Column<Guid>(nullable: false),
                    ValidUntil = table.Column<DateTime>(nullable: false),
                    Agent = table.Column<string>(maxLength: 63, nullable: true),
                    UserId = table.Column<Guid>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_US_SESSION", x => x.Id);
                    table.ForeignKey(
                        name: "FK_US_SESSION_US_USER_UserId",
                        column: x => x.UserId,
                        principalTable: "US_USER",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "US_GROUP_MEMBER",
                columns: table => new
                {
                    Id = table.Column<Guid>(nullable: false),
                    IsDeleted = table.Column<bool>(nullable: false, defaultValue: false),
                    GroupId = table.Column<Guid>(nullable: false),
                    UserId = table.Column<Guid>(nullable: false),
                    RoleId = table.Column<Guid>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_US_GROUP_MEMBER", x => x.Id);
                    table.ForeignKey(
                        name: "FK_US_GROUP_MEMBER_US_GROUP_GroupId",
                        column: x => x.GroupId,
                        principalTable: "US_GROUP",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_US_GROUP_MEMBER_US_GROUP_MEMBER_ROLE_RoleId",
                        column: x => x.RoleId,
                        principalTable: "US_GROUP_MEMBER_ROLE",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_US_GROUP_MEMBER_US_USER_UserId",
                        column: x => x.UserId,
                        principalTable: "US_USER",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "US_LINK_GROUP_MEMBER_ROLE_TO_PERMISSION",
                columns: table => new
                {
                    GroupMemberRoleId = table.Column<Guid>(nullable: false),
                    GroupMemberPermissionId = table.Column<Guid>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_US_LINK_GROUP_MEMBER_ROLE_TO_PERMISSION", x => new { x.GroupMemberRoleId, x.GroupMemberPermissionId });
                    table.ForeignKey(
                        name: "FK_US_LINK_GROUP_MEMBER_ROLE_TO_PERMISSION_US_GROUP_MEMBER_PERM~",
                        column: x => x.GroupMemberPermissionId,
                        principalTable: "US_GROUP_MEMBER_PERMISSION",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_US_LINK_GROUP_MEMBER_ROLE_TO_PERMISSION_US_GROUP_MEMBER_ROLE~",
                        column: x => x.GroupMemberRoleId,
                        principalTable: "US_GROUP_MEMBER_ROLE",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateIndex(
                name: "IX_US_GROUP_ParentGroupId",
                table: "US_GROUP",
                column: "ParentGroupId");

            migrationBuilder.CreateIndex(
                name: "IX_US_GROUP_MEMBER_GroupId",
                table: "US_GROUP_MEMBER",
                column: "GroupId");

            migrationBuilder.CreateIndex(
                name: "IX_US_GROUP_MEMBER_RoleId",
                table: "US_GROUP_MEMBER",
                column: "RoleId");

            migrationBuilder.CreateIndex(
                name: "IX_US_GROUP_MEMBER_UserId",
                table: "US_GROUP_MEMBER",
                column: "UserId");

            migrationBuilder.CreateIndex(
                name: "IX_US_GROUP_MEMBER_ROLE_GroupId",
                table: "US_GROUP_MEMBER_ROLE",
                column: "GroupId");

            migrationBuilder.CreateIndex(
                name: "IX_US_LINK_GROUP_MEMBER_ROLE_TO_PERMISSION_GroupMemberPermissio~",
                table: "US_LINK_GROUP_MEMBER_ROLE_TO_PERMISSION",
                column: "GroupMemberPermissionId");

            migrationBuilder.CreateIndex(
                name: "IX_US_SESSION_UserId",
                table: "US_SESSION",
                column: "UserId");

            migrationBuilder.CreateIndex(
                name: "IX_US_USER_UserName",
                table: "US_USER",
                column: "UserName",
                unique: true);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "US_GROUP_MEMBER");

            migrationBuilder.DropTable(
                name: "US_LINK_GROUP_MEMBER_ROLE_TO_PERMISSION");

            migrationBuilder.DropTable(
                name: "US_SESSION");

            migrationBuilder.DropTable(
                name: "US_GROUP_MEMBER_PERMISSION");

            migrationBuilder.DropTable(
                name: "US_GROUP_MEMBER_ROLE");

            migrationBuilder.DropTable(
                name: "US_USER");

            migrationBuilder.DropTable(
                name: "US_GROUP");
        }
    }
}
