﻿using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Logging;
using Microsoft.Extensions.Logging.Console;
using Rest.Models.User;

namespace Rest
{
    public abstract class BaseContext : DbContext
    {

        public static readonly ILoggerFactory MyLoggerFactory
        = LoggerFactory.Create(builder => { builder.AddConsole(); });

        protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
        {
            optionsBuilder
            .UseLoggerFactory(MyLoggerFactory)
            .EnableSensitiveDataLogging()
            .UseMySql(@"server=localhost;database=TRIALS;uid=root;password=trials-live-score;");
        }
    }
}
