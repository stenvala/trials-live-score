﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Storage.ValueConversion;
using Rest.Models.User;

namespace Rest
{
    public class UserContext : BaseContext
    {
        public DbSet<User> Users { get; set; }

        public DbSet<Session> Sessions { get; set; }

        public DbSet<Group> Groups { get; set; }
        public DbSet<GroupMember> GroupMembers { get; set; }
        public DbSet<GroupMemberRole> GroupMemberRoles { get; set; }
        public DbSet<GroupMemberPermission> GroupMemberPermissions { get; set; }
        public DbSet<GroupMemberRoleGroupMemberPermission> RoleToPermission { get; set; }


        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.Entity<User>()
                .HasIndex(e => e.UserName)
                .IsUnique();

            modelBuilder.Entity<Group>().Property(e => e.Type).HasConversion(new EnumToStringConverter<GroupType>());            

            modelBuilder.Entity<GroupMember>()
                .Property(m => m.IsDeleted)
                .HasDefaultValue(false);

            modelBuilder.Entity<GroupMemberRole>().Property(e => e.Type).HasConversion(new EnumToStringConverter<RoleType>());
            modelBuilder.Entity<GroupMemberRole>().Property(e => e.GroupType).HasConversion(new EnumToStringConverter<GroupType>());

            modelBuilder.Entity<GroupMemberPermission>().Property(e => e.Permission).HasConversion(new EnumToStringConverter<Permission>());


            /*
            modelBuilder.Entity<User>().ToTable("USER");
            modelBuilder.Entity<Group>().ToTable("GROUP");
            modelBuilder.Entity<GroupMember>().ToTable("GROUP_MEMBER");
            modelBuilder.Entity<GroupMemberRole>().ToTable("GROUP_MEMBER_ROLE");
            modelBuilder.Entity<GroupMemberPermission>().ToTable("GROUP_MEMBER_PERMISSION");
            modelBuilder.Entity<GroupMemberRoleGroupMemberPermission>().ToTable("GROUP_MEMBER_ROLE_TO_MEMBER_PERMISSION");
            */

            modelBuilder.Entity<GroupMemberRoleGroupMemberPermission>()
                .HasKey(g => new { g.GroupMemberRoleId, g.GroupMemberPermissionId});

            modelBuilder.Entity<GroupMemberRoleGroupMemberPermission>()
                .HasOne(g => g.GroupMemberRole)
                .WithMany(gg => gg.PermissionMap)
                .HasForeignKey(gg => gg.GroupMemberRoleId);

            modelBuilder.Entity<GroupMemberRoleGroupMemberPermission>()
                .HasOne(g => g.GroupMemberPermission)
                .WithMany(gg => gg.Roles)
                .HasForeignKey(g => g.GroupMemberPermissionId);                
        }

    }
}
