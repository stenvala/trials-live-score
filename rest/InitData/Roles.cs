﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;
using Rest.Models.User;

namespace Rest.InitData
{
    public class Roles
    {

        public async static Task CreateRoles()
        {
            // Back office admin
            var backOfficeAdmin = new GroupMemberRole()
            {
                IsGlobal = true,
                Type = RoleType.Admin,
                GroupType = GroupType.BackOffice,
            };
            // Normal group admin
            var normalGroupAdmin = new GroupMemberRole()
            {
                IsGlobal = true,
                Type = RoleType.Admin,
                GroupType = GroupType.Normal
            };
            // Normal group member
            var normalGroupMember = new GroupMemberRole()
            {
                IsGlobal = true,
                Type = RoleType.Member,
                GroupType = GroupType.Normal
            };
            var roles = new List<GroupMemberRole>()
            {
                backOfficeAdmin, normalGroupAdmin, normalGroupMember
            };
            // Create only those that are not yet in db
            var rolesToAttach = roles.Where(r =>
            {
                using (var db = new UserContext())
                {
                    var existingRole = (from temp in db.GroupMemberRoles
                                        where temp.Type == r.Type && temp.GroupType == r.GroupType
                                        select temp).Count();
                    return existingRole == 0;
                }
            }).ToList();            

            using (var db = new UserContext())
            {
                db.GroupMemberRoles.AddRange(rolesToAttach);
                await db.SaveChangesAsync();
            }
        }

        public async static Task CreatePermissions()
        {
            var permissions = Enum.GetValues(typeof(Permission)).Cast<Permission>();

            using (var db = new UserContext())
            {
                foreach (var perm in permissions)
                {
                    var count = (from temp in db.GroupMemberPermissions
                                 where temp.Permission == perm
                                 select temp).Count();
                    if (count == 0)
                    {
                        var toAdd = new GroupMemberPermission()
                        {
                            Permission = perm
                        };
                        db.GroupMemberPermissions.Add(toAdd);
                        await db.SaveChangesAsync();
                    }
                }
            }
        }

        public async static Task MapPermissionToRole()
        {
            using (var db = new UserContext())
            {
                var roleBackOfficeAdmin = await (from r in db.GroupMemberRoles
                                                 where r.Type == RoleType.Admin && r.GroupType
                                                 == GroupType.BackOffice
                                                 select r
                                  ).FirstOrDefaultAsync();
                var roleAdmin = await (from r in db.GroupMemberRoles
                                       where r.Type == RoleType.Admin && r.GroupType
                                       == GroupType.Normal
                                       select r
                               ).FirstOrDefaultAsync();
                var roleMember = await (from r in db.GroupMemberRoles
                                        where r.Type == RoleType.Member && r.GroupType
                                        == GroupType.Normal
                                        select r
                               ).FirstOrDefaultAsync();

                var permissions = new Dictionary<GroupMemberRole, List<Permission>>()
                {
                    { roleBackOfficeAdmin, new List<Permission>() {
                        Permission.AddMember,
                        Permission.CreateEvent,
                        Permission.DeleteEvent,
                        Permission.DeleteMember,
                        Permission.EditMember,
                        Permission.EditSealedEvent,
                        Permission.GetEvent,
                        Permission.GetGroup,
                        Permission.GetMember
                        }
                    },
                    {
                        roleAdmin, new List<Permission>
                        {
                            Permission.AddMember,
                            Permission.CreateEvent,
                            Permission.DeleteEvent,
                            Permission.DeleteMember,
                            Permission.EditMember,
                            Permission.EditSealedEvent,
                            Permission.GetEvent,
                            Permission.GetGroup,
                            Permission.GetMember,
                            Permission.InviteReferee,
                            Permission.RemoveReferee,
                            Permission.SealEvent,
                            Permission.UpdateEvent
                        }
                    },
                    {
                        roleMember, new List<Permission>
                        {
                            Permission.GetGroup,
                            Permission.GetEvent
                        }
                    }
                };

                var toAdd = new List<GroupMemberRoleGroupMemberPermission>();
                foreach (var entry in permissions)
                {                    
                    entry.Value.ForEach(p =>
                    {                        
                        var data = (from r in db.GroupMemberRoles
                                    join map in
                                    (from rtp in db.RoleToPermission
                                     join perm in db.GroupMemberPermissions on rtp.GroupMemberPermissionId equals perm.Id
                                     where perm.Permission == p
                                     select new
                                     {
                                         rtp,
                                         perm
                                     }) on r.Id equals map.rtp.GroupMemberRoleId
                                    where r.Id == entry.Key.Id
                                    select r).FirstOrDefault();
                        if (data == null)
                        {                                         
                            var perm = (from permRow in db.GroupMemberPermissions
                                        where permRow.Permission == p
                                        select permRow).FirstOrDefault();                            
                            var map = new GroupMemberRoleGroupMemberPermission()
                            {
                                GroupMemberPermissionId = perm.Id,
                                GroupMemberRoleId = entry.Key.Id
                            };                            
                            toAdd.Add(map);                            
                        }
                    });
                }                
                db.RoleToPermission.AddRange(toAdd);
                db.SaveChanges();
            }
        }

        public async static Task<object> GetRoleData()
        {
            using (var db = new UserContext())
            {
                var data = await db.GroupMemberRoles
                    .Include(r => r.PermissionMap)
                    .ThenInclude(p => p.GroupMemberPermission)
                    .ToListAsync();
                return data;
            }
        }
    }   
}
