﻿using System;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;
using Rest.Models.User;
using Rest.Providers;

namespace Rest.InitData
{
    public class Backoffice
    {

        public static async Task CreateBackOffice(Guid userId)
        {
            using (var db = new UserContext())
            {
                // Only one backoffice is allowed
                var id = await (from g in db.Groups
                                where g.Type == GroupType.BackOffice
                                select g.Id).FirstOrDefaultAsync();
                if (id != null)
                {
                    return;
                }
                var newGroup = new Group
                {
                    Name = "Backoffice",
                    Type = GroupType.BackOffice
                };

                var adminRole = await GroupFetch.GetGlobalRoleByType(RoleType.Admin, GroupType.BackOffice);
                db.Groups.Add(newGroup);

                var newGroupMember = new GroupMember()
                {
                    GroupId = newGroup.Id,
                    UserId = userId,
                    RoleId = adminRole.Id
                };
                db.GroupMembers.Add(newGroupMember);
                await db.SaveChangesAsync();
            }
        }

    }
}
