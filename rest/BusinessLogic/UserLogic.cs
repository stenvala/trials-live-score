﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using AutoMapper;
using Microsoft.EntityFrameworkCore;
using Rest.DTO;
using Rest.Models.User;
using Rest.Providers;
using Rest.Utils;

namespace Rest.BusinessLogic
{
    public interface IUserLogic
    {        
        Task<User> CreateUser(CreateableUser user, string password);
        Task<bool> IsUserNameFree(string userName);
        Task DeleteUserByUserName(string userName);
        Task<UserDTO> Login(string userName, string password);
        Task Logout(Guid sessionId);
        Task LogoutAll(Guid userId);
        Task<UserDTO> InitFromSession(Guid sessionId, bool withGroupData = true);
        Task<UserDTO> InitFromSession(Guid sessionId, Guid groupId);
        Task<UserDTO> InitForUserId(Guid userId, bool withGroupData = true);
        Task<UserDTO> FetchUserWithSessionGroupIdAndPermission(Guid sessionId, Guid groupId, Permission permission);        
    }

    public class UserLogic : IUserLogic
    {
        public IMapper Mapper;

        public UserLogic(IMapper mapper) {
            Mapper = mapper;
        }

        public async Task<User> CreateUser(CreateableUser user, string password)
        {            
            using (var db = new UserContext())
            {                
                if (!await IsUserNameFree(user.UserName))
                {
                    throw new AppException("Username already selected",
                        Error.USER_USERNAME_SELECTED, System.Net.HttpStatusCode.Conflict);
                }
                var newUser = new User
                {
                    GivenName = user.GivenName,
                    FamilyName = user.FamilyName,
                    UserName = user.UserName,
                    Email = user.Email,
                    PasswordHash = User.HashPassword(password),
                    CanUseOtherUsers = user.CanUseOtherUsers
                };

                db.Users.Add(newUser);
                await db.SaveChangesAsync();
                return newUser;
            }            
        }

        public async Task<bool> IsUserNameFree(string userName)
        {
            using (var db = new UserContext())
            {
                var guid = await (from u in db.Users
                                    where u.UserName == userName
                                    select u.Id).FirstOrDefaultAsync();
                if (guid != Guid.Empty)
                {
                    return false;
                }
                return true;
            }
        }

        public async Task DeleteUserByUserName(string userName)
        {
            using (var db = new UserContext())
            {
                var user = await (from u in db.Users
                                  where u.UserName == userName
                                  select u).FirstOrDefaultAsync();
                if (user != null)
                {
                    db.Users.Attach(user);
                    db.Users.Remove(user);
                    db.SaveChanges();
                }
            }
        }


        public async Task<UserDTO> Login(string userName, string password)
        {
            using (var db = new UserContext())
            {
                var user = await (from u in db.Users
                                          where u.UserName == userName
                                          select u).FirstOrDefaultAsync();
                if (user == null ||
                    !user.IsPasswordValid(password))
                {
                    throw new AppException("User not found",
                        Error.AUTH_FALSE_CREDENTIALS, System.Net.HttpStatusCode.BadRequest);
                }

                var session = new Session()
                {
                    UserId = user.Id,
                    ValidUntil = DateTime.UtcNow.AddMonths(3),
                    Agent = "Unknown"
                };
                user.Sessions = new List<Session>()
                {
                    session
                };
                await db.SaveChangesAsync();
                var userDTO = Mapper.Map<UserDTO>(user);
                userDTO.CurrentSession = Mapper.Map<SessionDTO>(session);
                return userDTO;
            }
        }

        public async Task Logout(Guid sessionId)
        {
            using (var db = new UserContext())
            {
                var session = await(from s in db.Sessions
                                 where s.Id == sessionId
                                 select s).FirstOrDefaultAsync();
                db.Sessions.Attach(session);
                db.Sessions.Remove(session);
                db.SaveChanges();
            }
        }

        public async Task LogoutAll(Guid userId)
        {
            using (var db = new UserContext())
            {
                // var sessions = await db.Users.Include(x => x.Sessions).ToListAsync();
                var sessions = await (from s in db.Sessions
                                      where s.UserId == userId
                                      select s).ToListAsync();
                sessions.Clear();
                db.SaveChanges();
            }
        }

        public async Task<UserDTO> InitFromSession(Guid sessionId, bool withGroupData = true)
        {
            using (var db = new UserContext())
            {
                var user = await UserFetch.ForSessionId(sessionId, db);
                if (user == null)
                {
                    return null;
                }
                if (withGroupData)
                {                    
                    user.MyGroups = await GroupFetch.FetchGroupsOfUser(user.Id, db);
                }
                return user;
            }
        }

        public async Task<UserDTO> InitFromSession(Guid sessionId, Guid groupId)
        {
            using (var db = new UserContext())
            {
                var user = await UserFetch.ForSessionId(sessionId, db);                
                user.CurrentGroup = await GroupFetch.FetchCurrentGroupOfUser(user.Id, groupId, db);                
                return user;
            }
        }

        public async Task<UserDTO> InitForUserId(Guid userId, bool withGroupData = true)
        {
            using (var db = new UserContext())
            {
                var user = await UserFetch.ForUserId(userId, db);
                if (withGroupData)
                {
                    user.MyGroups = await GroupFetch.FetchGroupsOfUser(user.Id, db);
                }
                return user;
            }
        }

        public async Task<UserDTO> FetchUserWithSessionGroupIdAndPermission(Guid sessionId, Guid groupId, Permission permission)
        {            
            var user = await InitFromSession(sessionId, groupId);         
            if (user == null)
            {
                return null;
            }         
            using (var db = new UserContext())
            {
                user.CurrentGroup = await GroupFetch.FetchCurrentGroupOfUser(user.Id, groupId, db);             
                if (user.CurrentGroup == null || !user.CurrentGroup.MyMembership.Permissions.Any(p => p == permission))
                {
                    return null;
                }
            }
            return user;
        }
    }
}
