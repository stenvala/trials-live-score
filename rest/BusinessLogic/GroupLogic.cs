﻿using System;
using System.Threading.Tasks;
using Rest.Models.User;
using Rest.Providers;
using System.Linq;
using Microsoft.EntityFrameworkCore;

namespace Rest.BusinessLogic
{
    public interface IGroupLogic
    {
        Task<Group> CreateGroup(string name, Guid userId);
        Task AddUserToGroup(Guid userId, Guid groupId, Guid roleId);
        Task<GroupMemberRole> GetRoleOfGroup(Guid roleId, Guid groupId);
        Task AssignRoleToMember(Guid roleId, Guid memberId);
    }

    public class GroupLogic : IGroupLogic
    {
        public GroupLogic()
        {
        }

        public async Task<Group> CreateGroup(string name, Guid userId)
        {
            using (var db = new UserContext())
            {
                var group = new Group
                {
                    Name = name,
                    Type = GroupType.Normal
                };

                var adminRole = await GroupFetch.GetGlobalRoleByType(RoleType.Admin, GroupType.Normal);
                db.Groups.Add(group);

                var membership = new GroupMember()
                {
                    GroupId = group.Id,
                    UserId = userId,
                    RoleId = adminRole.Id
                };
                db.GroupMembers.Add(membership);
                db.SaveChanges();
                return group;
            }            
        }

        public async Task AddUserToGroup(Guid userId, Guid groupId, Guid roleId)
        {
            using (var db = new UserContext())
            {
                var newGroupMember = new GroupMember()
                {
                    GroupId = groupId,
                    UserId = userId,
                    RoleId = roleId
                };
                db.GroupMembers.Add(newGroupMember);

                await db.SaveChangesAsync();
            }
        }

        public async Task<GroupMemberRole> GetRoleOfGroup(
            Guid roleId, Guid groupId)
        {
            return await GroupFetch.GetRoleOfGroup(roleId, groupId);
        }

        public async Task AssignRoleToMember(
            Guid roleId, Guid memberId)
        {
            using (var db = new UserContext())
            {                
                var member = await (
                    from gm in db.GroupMembers
                    where gm.Id == memberId
                    select gm).FirstOrDefaultAsync();
                db.GroupMembers.Attach(member);
                member.RoleId = roleId;
                await db.SaveChangesAsync();
            }
        }

    }
}
