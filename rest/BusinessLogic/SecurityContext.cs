﻿using System;
using System.Collections.Generic;
using Rest.DTO;
using Rest.Models.User;
using Rest.Utils;

namespace Rest.BusinessLogic
{
    public class JWTPermissionData
    {        
        public Guid UserId { get; set; }
        public string GivenName { get; set; }
        public string FamilyName { get; set; }
        public Dictionary<Guid, GroupData> GroupData { get; set; }
    }

    public class GroupData
    {
        public IList<Permission> Permissions { get; set; }
        public Guid MembershipId { get; set; }
    }

    public interface ISecurityContext
    {
        UserDTO GetUser();
        Guid GetUserId();
        UserDTO SetUser(UserDTO user);        
        MyGroupDTO GetSelectedGroup();
        MyMembershipDTO GetSelectedGroupMember();
        Guid GetSelectedGroupId();
        Guid GetSelectedGroupMembershipId();
        void SetSelectedGroupId(Guid id);        
        void SetJWTData(JWTPermissionData data);
    }

    abstract public class JWTPermissions
    {
        private static readonly string KEY_PERMISSIONS = "PERMISSIONS";
        public static readonly string COOKIE_PERMISSIONS = "JWT_PERMISSIONS";

        public static JWTTokenData GetTokenForUser(UserDTO user)
        {
            var data = new JWTPermissionData()
            {
                UserId = user.Id,
                GivenName = user.GivenName,
                FamilyName = user.FamilyName,
                GroupData = new Dictionary<Guid, GroupData>()
            };
            foreach (var group in user.MyGroups)
            {
                data.GroupData.Add(group.Id, new GroupData()
                {
                    Permissions = group.MyMembership.Permissions,
                    MembershipId = group.Id
                });
            }
            return JWT.Encode(KEY_PERMISSIONS, data);
        }

        public static JWTPermissionData GetDataFromToken(string token)
        {
            return JWT.Decode<JWTPermissionData>(KEY_PERMISSIONS, token);
        }
    }

    public class SecurityContext : ISecurityContext
    {        

        private UserDTO User { get; set; }
        private Guid SelectedGroupId { get; set; }
        private JWTPermissionData Data { get; set; }

        public UserDTO GetUser()
        {
            return User;
        }

        public Guid GetUserId()
        {
            if (User == null)
            {
                return Data.UserId;
            }
            return User.Id;
        }

        public UserDTO SetUser(UserDTO user)
        {
            User = user;
            return User;
        }
        
        public MyGroupDTO GetSelectedGroup()
        {
            return User == null ? null : User.CurrentGroup;
        }

        public MyMembershipDTO GetSelectedGroupMember()
        {
            return User == null ? null : User.CurrentGroup.MyMembership;
        }

        public void SetSelectedGroupId(Guid id)
        {
            SelectedGroupId = id;
        }

        public Guid GetSelectedGroupId()
        {
            if (SelectedGroupId != null)
            {
                return SelectedGroupId;
            }
            var group = GetSelectedGroup();
            if(group != null)
            {
                return group.Id;
            }
            return Guid.Empty;
        }

        public void SetJWTData(JWTPermissionData data)
        {
            Data = data;
        }

        public Guid GetSelectedGroupMembershipId()
        {
            var group = GetSelectedGroup();
            if (group != null)
            {
                return group.MyMembership.Id;
            }
            // is necessarily in jwt, otherwise this has failed
            // When transitive permissions are in use, fix this
            var groupId = GetSelectedGroupId();
            return Data.GroupData[groupId].MembershipId;
        }        
    }
}
