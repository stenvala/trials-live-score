﻿using System;
using System.Collections.Generic;

namespace Rest.DTO
{
    public class CollectionDTO<T>
    {
        public ICollection<T> Results;
        public int TotalCount;
        public int Count;
        public int From;        
    }
}
