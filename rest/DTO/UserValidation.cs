﻿using System.Threading.Tasks;
using FluentValidation;
using Rest.BusinessLogic;
using Rest.DTO;
using Rest.Utils;

public class RegisterValidator : AbstractValidator<RegisterDTO>
{    

    public RegisterValidator(IUserLogic userLogic)
    { 
        RuleFor(x => x.GivenName).NotEmpty();
        RuleFor(x => x.FamilyName).NotEmpty();
        RuleFor(x => x.Password).NotEmpty().MinimumLength(8);
        RuleFor(x => x.Email).NotEmpty().Must(Validation.IsEmailValid).WithErrorCode("InvalidEmail");
        RuleFor(x => x.UserName).NotEmpty().MustAsync((x, cancellation) => userLogic.IsUserNameFree(x)).WithErrorCode("UserNameTaken");
    }    
    
}
