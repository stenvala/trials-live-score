﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using Rest.Models.User;

namespace Rest.DTO
{
    public class RegisterDTO
    {        
        public string GivenName { get; set; }        
        public string FamilyName { get; set; }        
        public string UserName { get; set; }        
        public string Email { get; set; }        
        public string Password { get; set; }
    }        

    public class LoginDTO
    {
        public string UserName { get; set; }
        public string Password { get; set; }
    }

    public class UserDTO
    {
        public Guid Id { get; set; }
        public string GivenName { get; set; }
        public string FamilyName { get; set; }
        public string UserName { get; set; }
        public string Email { get; set; }
        public bool CanUseOtherUsers { get; set; }
        public IList<MyGroupDTO> MyGroups { get; set; }
        public MyGroupDTO CurrentGroup { get; set; }
        public SessionDTO CurrentSession { get; set; }
    }

    public class SessionDTO
    {
        public DateTime ValidUntil { get; set; }
        public Guid Id { get; set; }
    }

    public class PlainGroupDTO
    {
        public Guid Id { get; set; }
        public string Name;
        public GroupType Type { get; set; }
    }

    public class MyGroupDTO : PlainGroupDTO
    {             
        public Guid? ParentGroupId { get; set; }
        public IList<PlainGroupDTO> ChildGroups { get; set; }
        public MyMembershipDTO MyMembership { get; set; }
    }

    public class MyMembershipDTO
    {
        public Guid Id { get; set; }        
        public RoleType RoleType;
        public string RoleName;
        public IList<Permission> Permissions { get; set; }
    }    
    
}
