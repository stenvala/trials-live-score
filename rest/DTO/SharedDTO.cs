﻿using System;
using System.Collections.Generic;

namespace Rest.DTO
{
    public class ValidationErrorDTO
    {
        public string PropertyName { get; set; }
        public string ErrorMessage { get; set; }
        public string AttemptedValue { get; set; }
        public string ErrorCode { get; set; }
    }

    public class ValidationErrorsDTO
    {
        public IList<ValidationErrorDTO> ValidationErrors { get; set; }
        public bool Failed { get { return true; } }
    }
}
