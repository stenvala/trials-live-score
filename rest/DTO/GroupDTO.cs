﻿using System;
using System.Collections.Generic;
using Rest.Models.User;
using Rest.Providers;

namespace Rest.DTO
{
    public class GroupMemberDTO
    {
        public Guid Id { get; set; }
        public string GivenName { get; set; }        
        public string FamilyName { get; set; }
        public RoleType RoleType;
        public string RoleName;
    }    

    public class RoleDTO
    {
        public Guid Id { get; set; }
        public bool IsGlobal { get; set; }
        public string Name { get; set; }
        public RoleType Type { get; set; }
        public IList<Permission> Permissions { get; set; }
    }
    
}
