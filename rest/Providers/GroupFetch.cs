﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;
using Rest.DTO;
using Rest.Models.User;

namespace Rest.Providers
{    
    public class GroupFetch
    {
        public static async Task<GroupMemberRole> GetGlobalRoleByType(RoleType roleType, GroupType groupType)
        {
            using (var db = new UserContext())
            {
                return await (from r in db.GroupMemberRoles
                              where r.Type == roleType && r.GroupType == groupType && r.IsGlobal
                              select r).FirstOrDefaultAsync();
            }
        }

        public static async Task<Group> FindGroupByName(string name)
        {
            using (var db = new UserContext())
            {
                return await (from g in db.Groups
                                   where g.Name == name
                                   select g).FirstOrDefaultAsync();
                    
            }
        }        

        public static async Task<IList<MyGroupDTO>> FetchGroupsOfUser(Guid userId, UserContext db)
        {
            return await (from gm in db.GroupMembers
                          join r in db.GroupMemberRoles on gm.RoleId equals r.Id
                          join g in db.Groups on gm.GroupId equals g.Id
                          where gm.UserId == userId && !gm.IsDeleted
                          select new MyGroupDTO
                          {
                              Id = g.Id,
                              Name = g.Name,
                              Type = g.Type,
                              ParentGroupId = g.ParentGroupId,
                              ChildGroups = (from cg in db.Groups
                                             where cg.ParentGroupId == g.Id
                                             select new PlainGroupDTO()
                                             {
                                                 Name = cg.Name,
                                                 Id = cg.Id
                                             }).ToList(),
                              MyMembership = new MyMembershipDTO()
                              {
                                  Id = gm.Id,
                                  RoleType = r.Type,
                                  RoleName = r.Name,
                                  Permissions = (
                                      from perm in db.GroupMemberPermissions
                                      from map in db.RoleToPermission
                                      where map.GroupMemberRoleId == r.Id &&
                                      perm.Id == map.GroupMemberPermissionId
                                      select perm.Permission
                                      ).ToList()
                              }
                          }).ToListAsync();
        }

        public static async Task<MyGroupDTO> FetchCurrentGroupOfUser(Guid userId, Guid groupId, UserContext db)
        {
            return await (from gm in db.GroupMembers
                          join r in db.GroupMemberRoles on gm.RoleId equals r.Id
                          join g in db.Groups on gm.GroupId equals g.Id
                          where gm.UserId == userId && gm.GroupId == groupId && !gm.IsDeleted
                          select new MyGroupDTO
                          {
                              Id = g.Id,
                              Name = g.Name,
                              Type = g.Type,
                              ParentGroupId = g.ParentGroupId,
                              ChildGroups = (from cg in db.Groups
                                             where cg.ParentGroupId == g.Id
                                             select new PlainGroupDTO()
                                             {
                                                 Name = cg.Name,
                                                 Id = cg.Id
                                             }).ToList(),
                              MyMembership = new MyMembershipDTO()
                              {
                                  Id = gm.Id,
                                  RoleType = r.Type,
                                  RoleName = r.Name,
                                  Permissions = (
                                      from perm in db.GroupMemberPermissions
                                      from map in db.RoleToPermission
                                      where map.GroupMemberRoleId == r.Id &&
                                      perm.Id == map.GroupMemberPermissionId
                                      select perm.Permission
                                      ).ToList()
                              }
                          }).FirstOrDefaultAsync();
        }

        public static async Task<CollectionDTO<GroupMemberDTO>> FetchGroupMembers(Guid groupId)
        {
            return await GroupMemberFetch(groupId, Guid.Empty);
        }

        public static async Task<GroupMemberDTO> FetchGroupMember(Guid groupId, Guid memberId)
        {
            return (await GroupMemberFetch(groupId, memberId)).Results.FirstOrDefault();            
        }

        public static async Task<Guid> GetGroupIdForMemberId(Guid memberId)
        {
            using (var db = new UserContext())
            {
                return await (from gm in db.GroupMembers
                              join g in db.Groups on gm.GroupId equals g.Id
                              where gm.Id == memberId && !gm.IsDeleted
                              select g.Id).FirstOrDefaultAsync();
            }
        }

        private static async Task<CollectionDTO<GroupMemberDTO>> GroupMemberFetch(Guid groupId, Guid memberId)
        {
            using (var db = new UserContext())
            {
                var members = await (from gm in db.GroupMembers
                              join r in db.GroupMemberRoles on gm.RoleId equals r.Id
                              join u in db.Users on gm.UserId equals u.Id
                              where gm.GroupId == groupId && (memberId == Guid.Empty || memberId == gm.Id) &&
                                !gm.IsDeleted
                              select new GroupMemberDTO
                              {
                                  Id = gm.Id,
                                  GivenName = u.GivenName,
                                  FamilyName = u.FamilyName,
                                  RoleType = r.Type,
                                  RoleName = r.Name
                              }).ToListAsync();
                return new CollectionDTO<GroupMemberDTO>()
                {
                    Results = members,
                    TotalCount = members.Count,
                    Count = members.Count,
                    From = 0
                };
            }
        }

        public static async Task<CollectionDTO<RoleDTO>> FetchGroupMemberRoles(Guid groupId)
        {
            using (var db = new UserContext())
            {
                var groupType = await GetGroupType(db, groupId);

                var roles = await (from r in db.GroupMemberRoles                                     
                                     where r.GroupType == groupType && (
                                        r.IsGlobal || (r.Type == RoleType.Custom && r.GroupId == groupId)) 
                                     select new RoleDTO
                                     {
                                         Id = r.Id,
                                         IsGlobal = r.IsGlobal,
                                         Name = r.Name,
                                         Type = r.Type,
                                         Permissions = (
                                            from perm in db.GroupMemberPermissions
                                            from map in db.RoleToPermission
                                            where map.GroupMemberRoleId == r.Id &&
                                            perm.Id == map.GroupMemberPermissionId
                                            select perm.Permission
                                            ).ToList()
                                     }).ToListAsync();
                return new CollectionDTO<RoleDTO>()
                {
                    Results = roles,
                    TotalCount = roles.Count,
                    Count = roles.Count,
                    From = 0
                };
            }
        }

        private static async Task<GroupType> GetGroupType(UserContext db, Guid groupId)
        {
            return await (from g in db.Groups
                   where g.Id == groupId
                   select g.Type).FirstOrDefaultAsync();
        }

        public static async Task<GroupMemberRole> GetRoleOfGroup(Guid roleId, Guid groupId)
        {
            using (var db = new UserContext()) {

                var groupType = await GetGroupType(db, groupId);

                return await (
                    from r in db.GroupMemberRoles
                    where r.Id == roleId &&
                    (r.GroupId == groupId || (r.IsGlobal && r.GroupType == groupType))
                    select r).FirstOrDefaultAsync();
            }
        }
    }
}
