﻿using System;
using System.Threading.Tasks;
using Rest.DTO;
using System.Linq;
using System.Collections.Generic;
using Microsoft.EntityFrameworkCore;

namespace Rest.Providers
{
    public class UserFetch
    {
  
        public async static Task<UserDTO> ForSessionId(Guid sessionId, UserContext db)
        {
            return await (from u in db.Users
                        from s in db.Sessions
                        where s.Id == sessionId && s.UserId == u.Id
                        select new UserDTO
                        {
                          Id = u.Id,
                          GivenName = u.GivenName,
                          FamilyName = u.FamilyName,
                          UserName = u.UserName,
                          Email = u.Email,
                          CanUseOtherUsers = u.CanUseOtherUsers,                          
                          CurrentSession = new SessionDTO
                          {
                            ValidUntil = s.ValidUntil,
                            Id = s.Id
                          }
                        }
                    ).FirstOrDefaultAsync();
        }

        public async static Task<UserDTO> ForUserId(Guid userId, UserContext db)
        {
          return await (from u in db.Users                    
                        where u.Id == userId
                        select new UserDTO
                        {
                          Id = u.Id,
                          GivenName = u.GivenName,
                          FamilyName = u.FamilyName,
                          UserName = u.UserName,
                          Email = u.Email,
                          CanUseOtherUsers = u.CanUseOtherUsers,
                        }
                  ).FirstOrDefaultAsync();
        }     
    }
}