﻿using System;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Rest.BusinessLogic;

namespace Rest.Utils
{
    public class UserResolver
    {

        private readonly RequestDelegate Next;        

        public UserResolver(RequestDelegate next)
        {
            Next = next;         
        }
        
        public async Task InvokeAsync(HttpContext context,
            ISecurityContext securityContext,
            IUserLogic userLogic)
        {
            var cookie = context.Request.Cookies["SESSION_ID"];
            if (cookie != null)
            {
                var user = await userLogic.InitFromSession(Guid.Parse(cookie));
                securityContext.SetUser(user);
            }
            await Next(context);            
        }        
    }
}
