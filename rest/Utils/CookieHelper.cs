﻿using System;
namespace Rest.Utils
{
    public class CookieHelper
    {
        public static readonly string KEY_SESSION = "SESSION_ID";
        public static readonly string KEY_JWT_PERMISSIONS = "JWT_PERMISSIONS";        
    }
}
