﻿using System;
using System.Collections.Generic;
using System.IdentityModel.Tokens.Jwt;
using System.Security.Claims;
using System.Text;
using Microsoft.IdentityModel.Tokens;
using Newtonsoft.Json;

namespace Rest.Utils
{    

    public struct JWTTokenData
    {
        public JWTTokenData(string token, DateTime expires)
        {
			Token = token;
			Expires = expires;
        }
		public string Token { get; }
        public DateTime Expires { get; } 
    }
   
    public class JWT
    {
		private static readonly string SECRET = "20db598f-4d66-48cc-a82b-9eeb6c933021";		
        
        public static JWTTokenData Encode(string key, object data)
        {
			var str = Jsoner.Convert(data);
			var now = DateTime.UtcNow;
			var expires = now.AddMinutes(10);
			return new JWTTokenData(
                CreateToken(key, str, expires),
                expires
				);			
		}

		public static T Decode<T>(string key, string token)
		{
			var claims = DecodeToken(token);
			var json = claims.FindFirst(key).Value;
            return JsonConvert.DeserializeObject<T>(json);
		}

        // Helpers
		private static string CreateToken(string key, string value, DateTime expires)
		{
			var claim = new Claim(key, value);
			return CreateToken(claim, expires);
		}

		private static string CreateToken(Claim claim, DateTime expires)
		{
			return CreateToken(new List<Claim>() { claim }, expires);
		}

		private static string CreateToken(IEnumerable<Claim> claims, DateTime expires)
		{
			var tokenHandler = new JwtSecurityTokenHandler();
			var tokenDescriptor = new SecurityTokenDescriptor
			{
				Subject = new ClaimsIdentity(claims),
				Expires = expires,
				SigningCredentials = new SigningCredentials(new SymmetricSecurityKey(Encoding.UTF8.GetBytes(SECRET)), SecurityAlgorithms.HmacSha256),
			};
			var token = tokenHandler.CreateToken(tokenDescriptor);
			var tokenString = tokenHandler.WriteToken(token);
			return tokenString;
		}

        private static ClaimsPrincipal DecodeToken(string token)
        {			
			var key = Encoding.ASCII.GetBytes(SECRET);
			var handler = new JwtSecurityTokenHandler();
			var validations = new TokenValidationParameters
			{
				ValidateIssuerSigningKey = true,
				IssuerSigningKey = new SymmetricSecurityKey(key),
				ValidateIssuer = false,
				ValidateAudience = false
			};
			return handler.ValidateToken(token, validations, out var tokenSecure);
		}
    }
}
