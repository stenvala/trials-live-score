﻿using System;
using System.Net;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Newtonsoft.Json;
using Newtonsoft.Json.Converters;

namespace Rest.Utils
{

    public enum Error
    {
        AUTH_FALSE_CREDENTIALS,
        AUTH_NOT_AUTHORIZED,
        USER_USERNAME_SELECTED,
        UNDEFINED
    }

    public class ExceptionDTO
    {
        [JsonConverter(typeof(StringEnumConverter))]
        public Error kind;
        public string message;
        public object data;
    }

    public class AppException : Exception
    {
        public Error Kind;
        public HttpStatusCode StatusCode;

        public AppException(string msg, Error kind, HttpStatusCode code) : base(msg)
        {
            Kind = kind;
            StatusCode = code;
        }
    }

    public class ErrorHandlingMiddleware
    {
        private readonly RequestDelegate Next;

        public ErrorHandlingMiddleware(RequestDelegate next)
        {
            Next = next;
        }

        public async Task InvokeAsync(HttpContext context)
        {
            try
            {                
                await Next(context);
            }
            catch (Exception ex)
            {             
                await HandleExceptionAsync(context, ex);
            }
        }

        private static Task HandleExceptionAsync(HttpContext context, Exception ex)
        {
            var statusCode = HttpStatusCode.InternalServerError;
            var response = new ExceptionDTO
            {
                message = ex.Message,
                kind = Error.UNDEFINED
            };
            if (ex.Data != null)
            {
                response.data = ex.Data;
            }
            if (ex is AppException)
            {
                response.kind = ((AppException)ex).Kind;
                statusCode = ((AppException)ex).StatusCode;
            } else if (ex.Message.IndexOf("AUTHENTICATION_FAILED", StringComparison.InvariantCulture) > 0)
            {
                response.kind = Error.AUTH_NOT_AUTHORIZED;
                response.message = "Request not authorized";
                statusCode = HttpStatusCode.Forbidden;
            } else
            {
                throw ex;
            }            
            var result = JsonConvert.SerializeObject(response);
            context.Response.ContentType = "application/json";
            context.Response.StatusCode = (int)statusCode;
            return context.Response.WriteAsync(result);            
        }
    }
}
