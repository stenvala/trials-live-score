﻿using AutoMapper;
using FluentValidation.Results;
using Rest.DTO;
using Rest.Models.User;

namespace Rest
{
    public class MappingProfile : Profile
    {
        public MappingProfile()
        {
            CreateMap<User, UserDTO>();
            CreateMap<Session, SessionDTO>();
            CreateMap<RegisterDTO, CreateableUser>();
            CreateMap<ValidationResult, ValidationErrorsDTO>()
                .ForMember(dest => dest.ValidationErrors, opt => opt.MapFrom(src => src.Errors));
            CreateMap<ValidationFailure, ValidationErrorDTO>();

        }
    }
}