﻿using System;
using System.ComponentModel.DataAnnotations;
namespace Rest.Utils
{
    static public class Validation
    {

        static public bool IsEmailValid(string email)
        {
            return new EmailAddressAttribute().IsValid(email);                
        }
    }
}
