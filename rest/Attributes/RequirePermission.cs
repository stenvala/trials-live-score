﻿using System;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Filters;
using Microsoft.Extensions.DependencyInjection;
using Rest.BusinessLogic;
using Rest.DTO;
using Rest.Models.User;
using Rest.Providers;
using Rest.Utils;
using static Rest.Models.User.GroupMemberPermission;

namespace Rest.Attributes
{    

    // User
    public class RequireUserAttribute : TypeFilterAttribute
    {
        public RequireUserAttribute() : base(typeof(RequireUserFilter))
        {
        }
    }

    public class RequireUserFilter : IAsyncAuthorizationFilter
    {            
        public async Task OnAuthorizationAsync(AuthorizationFilterContext context)
        {
            var securityContext = context.HttpContext.RequestServices.GetService<ISecurityContext>();
            var userLogic = context.HttpContext.RequestServices.GetService<IUserLogic>();

            var cookie = context.HttpContext.Request.Cookies[CookieHelper.KEY_SESSION];
            if (cookie != null)
            {                
                var user = await userLogic.InitFromSession(Guid.Parse(cookie));
                if (user == null)
                {
                    context.Result = new ForbidResult("AUTHENTICATION_FAILED");
                }
                securityContext.SetUser(user);
            }            
        }
        
    }

    // Group permissions
    public enum MemberQuery
    {
        Member,
        Identity
    }

    public class PermissionData
    {
        public Permission Permission;
        public MemberQuery Query;
        public string IdParamName;
        public Boolean AllowJWT;

        public PermissionData(Permission permission, MemberQuery query, string idParamName, bool allowJWT)
        {
            Permission = permission;
            Query = query;
            IdParamName = idParamName;
            AllowJWT = allowJWT;
        }        
    }

    public class RequirePermissionAttribute : TypeFilterAttribute
    {        
        public RequirePermissionAttribute(Permission permission,
            MemberQuery query,
            string idParamName,
            bool allowJWT = false
            ) : base(typeof(RequirePermissionFilter))
        {
            Arguments = new object[]
            {
                new PermissionData(permission, query, idParamName, allowJWT)
            };            
        }
    }


    public class RequirePermissionFilter : IAsyncAuthorizationFilter
    {
        readonly PermissionData Permission;
        private IUserLogic UserLogic;
        private AuthorizationFilterContext Context;
        private ISecurityContext SecurityContext;
        private Guid SessionId;

        public RequirePermissionFilter(PermissionData permission)
        {
            Permission = permission;
        }        

        public async Task OnAuthorizationAsync(AuthorizationFilterContext context)
        {
            Inject(context);            
            var idString = context.HttpContext.Request.RouteValues[Permission.IdParamName];
            var id = Guid.Parse(idString.ToString());

            if (await IsJWTPermissionTokenValid(id, context))
            {
                return;
            }
            // Fallback to user check
            UserDTO user = null;
            switch (Permission.Query)
            {
                case MemberQuery.Identity:
                    user = await QueryIdentity(id);                    
                    break;                
            }
            if (user == null)
            {
                context.Result = new ForbidResult("AUTHENTICATION_FAILED");
            }
            else
            {
                SecurityContext.SetUser(user);
            }
        }

        private async Task<bool> IsJWTPermissionTokenValid(Guid id, AuthorizationFilterContext context)
        {
            if (Permission.AllowJWT)
            {
                var token = context.HttpContext.Request.Cookies[CookieHelper.KEY_JWT_PERMISSIONS];
                if (token != null)
                {
                    var groupId = await MapIdToGroupId(id);
                    var data = JWTPermissions.GetDataFromToken(token);
                    SecurityContext.SetSelectedGroupId(groupId);
                    SecurityContext.SetJWTData(data);
                    return data.GroupData.ContainsKey(groupId) &&
                        data.GroupData[groupId].Permissions.Any(perm => perm == Permission.Permission);
                }

            }
            return false;
        }

        private async Task<Guid> MapIdToGroupId(Guid id)
        {
            switch (Permission.Query)
            {
                case MemberQuery.Identity:
                    return id;                    
                case MemberQuery.Member:
                    return await GroupFetch.GetGroupIdForMemberId(id);
            }
            return Guid.Empty;
        }


        private void Inject(AuthorizationFilterContext context)
        {
            Context = context;
            SecurityContext = context.HttpContext.RequestServices.GetService<ISecurityContext>();
            UserLogic = context.HttpContext.RequestServices.GetService<IUserLogic>();
            var sessionId = context.HttpContext.Request.Cookies[CookieHelper.KEY_SESSION];
            if (sessionId != null)
            {
                SessionId = Guid.Parse(sessionId);
            }            
        }        
        private async Task<UserDTO> QueryIdentity(Guid groupId)
        {
            return await UserLogic.FetchUserWithSessionGroupIdAndPermission(SessionId, groupId, Permission.Permission);
        }        
    }
}

