﻿using System;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Rest.Attributes;
using Rest.BusinessLogic;
using Rest.Models.User;
using Rest.Providers;
using Rest.Utils;

namespace Rest.Controllers
{
    [Route("api/group")]
    public class GroupMemberController : Controller
    {

        private readonly IGroupLogic GroupLogic;        
        private readonly ISecurityContext SecurityContext;

        public GroupMemberController(
            IGroupLogic groupLogic,
            ISecurityContext securityContext)
        {
            GroupLogic = groupLogic;            
            SecurityContext = securityContext;            
        }        

        [RequirePermission(Permission.GetGroup, MemberQuery.Identity, "groupId")]
        [HttpGet("{groupId}")]
        public string GetGroup()
        {
            var group = SecurityContext.GetSelectedGroup();
            return Jsoner.Convert(group);
        }

        [RequirePermission(Permission.GetMember, MemberQuery.Identity, "groupId", true)]
        [HttpGet("{groupId}/members")]
        public async Task<string> GetGroupMembers(string groupId)
        {
            var members = await GroupFetch.FetchGroupMembers(Guid.Parse(groupId));
            return Jsoner.Convert(members);
        }

        [RequirePermission(Permission.GetGroup, MemberQuery.Identity, "groupId", true)]
        [HttpGet("{groupId}/roles")]
        public async Task<string> GroupGroupRoles(string groupId)
        {
            var members = await GroupFetch.FetchGroupMemberRoles(Guid.Parse(groupId));
            return Jsoner.Convert(members);
        }

        [RequirePermission(Permission.EditMember, MemberQuery.Member, "memberId", true)]
        [HttpPut("member/{memberId}/assign-role/{roleId}")]
        public async Task<string> SetRoleToMember(string memberId, string roleId)
        {
            var groupId = SecurityContext.GetSelectedGroupId();
            var role = await GroupLogic.GetRoleOfGroup(Guid.Parse(roleId), groupId);
            await GroupLogic.AssignRoleToMember(role.Id, Guid.Parse(memberId));
            var member = await GroupFetch.FetchGroupMember(groupId, Guid.Parse(memberId));
            return Jsoner.Convert(member);
        }
    }
}
