﻿using System;
using System.Net;
using System.Threading.Tasks;
using AutoMapper;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Rest.Attributes;
using Rest.BusinessLogic;
using Rest.DTO;
using Rest.Models.User;
using Rest.Utils;

namespace Rest.Controllers
{
    [Route("api/user")]
    public class UserController : Controller
    {
        
        private readonly IUserLogic UserLogic;        
        private readonly ISecurityContext SecurityContext;
        private readonly IMapper Mapper;

        public UserController(
            IUserLogic userLogic,
            IMapper mapper,
            ISecurityContext securityContext)
        {
            UserLogic = userLogic;
            Mapper = mapper;
            SecurityContext = securityContext;         
        }

        [HttpPost("register")]
        public async Task<string> PostRegister([FromBody] RegisterDTO user)
        {
            var validator = new RegisterValidator(UserLogic);
            var validationResult = await validator.ValidateAsync(user);
            if (!validationResult.IsValid)
            {
                Response.StatusCode = (int) HttpStatusCode.BadRequest;
                var response = Mapper.Map<ValidationErrorsDTO>(validationResult);                
                return Jsoner.Convert(response);                
            }
            var createableUser = Mapper.Map<CreateableUser>(user);
            createableUser.CanUseOtherUsers = false;
            var createdUser = UserLogic.CreateUser(createableUser, user.Password);
            return Jsoner.Convert(createdUser);
        }

        [HttpPost("login")]
        public async Task<string> PostLogin([FromBody] LoginDTO credentials)
        {
            var user = await UserLogic.Login(credentials.UserName,
                credentials.Password);
            var session = user.CurrentSession;
            CookieOptions options = new CookieOptions();
            options.Expires = session.ValidUntil;
            Response.Cookies.Append(CookieHelper.KEY_SESSION, session.Id.ToString(), options);
            return Jsoner.Convert(user);
        }

        [RequireUser()]
        [HttpGet("logout")]
        public async Task<string> GetLogout()
        {
            var cookie = Request.Cookies[CookieHelper.KEY_SESSION];
            await UserLogic.Logout(Guid.Parse(cookie));
            return Jsoner.Ok();
        }

        [RequireUser()]
        [HttpGet("logout-all")]
        public async Task<string> GetLogoutAll()
        {
            var cookie = Request.Cookies[CookieHelper.KEY_SESSION];
            var user = await UserLogic.InitFromSession(Guid.Parse(cookie));
            await UserLogic.LogoutAll(user.Id);
            return Jsoner.Ok();
        }

        [RequireUser()]
        [HttpGet("me")]
        public string GetMe()
        {
            var user = SecurityContext.GetUser();
            var token = JWTPermissions.GetTokenForUser(user);
            CookieOptions options = new CookieOptions();
            options.Expires = token.Expires;
            Response.Cookies.Append(CookieHelper.KEY_JWT_PERMISSIONS, token.Token, options);
            return Jsoner.Convert(user);
        }        
    }
}
