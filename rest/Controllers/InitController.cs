﻿using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Rest.BusinessLogic;
using Rest.InitData;
using Rest.Models.User;
using Rest.Providers;
using Rest.Utils;

namespace Rest.Controllers
{
    [Route("api/init")]
    public class InitController : Controller
    {

        private readonly IGroupLogic GroupLogic;
        private readonly IUserLogic UserLogic;        

        public InitController(
            IGroupLogic groupLogic, IUserLogic userLogic)
        {
            GroupLogic = groupLogic;
            UserLogic = userLogic;            
        }

        [HttpGet("create-roles")]
        public async Task<string> InitData()
        {
            await Roles.CreateRoles();
            await Roles.CreatePermissions();
            await Roles.MapPermissionToRole();
            var data = await Roles.GetRoleData();
            return Jsoner.Convert(data);
        }

        [HttpGet("create-sysadmin")]
        public async Task<string> GetCreateDemoUser()
        {
            var demoUser = new CreateableUser()
            {
                GivenName = "Antti",
                FamilyName = "Stenvall",
                UserName = "stenvala",
                Email = "antti@stenvall.fi",
                CanUseOtherUsers = true
            };
            var password = "testipesti";
            var user = await UserLogic.CreateUser(demoUser, password);
            await Backoffice.CreateBackOffice(user.Id);
            await GroupLogic.CreateGroup("Testi pesti", user.Id);
            var fullUser = await UserLogic.InitForUserId(user.Id, true);
            return Jsoner.Convert(fullUser);
        }

        [HttpGet("create-helper")]
        public async Task<string> GetOtherAndAdd()
        {
            var demoUser = new CreateableUser()
            {
                GivenName = "Test",
                FamilyName = "User",
                UserName = "test",
                Email = "test@user.fi",
                CanUseOtherUsers = false
            };
            var password = "testipesti";
            var user = await UserLogic.CreateUser(demoUser, password);
            var group = await GroupFetch.FindGroupByName("Testi pesti");
            var role = await GroupFetch.GetGlobalRoleByType(RoleType.Member,
                group.Type);
            await GroupLogic.AddUserToGroup(user.Id, group.Id, role.Id);
            var fullUser = await UserLogic.InitForUserId(user.Id, true);
            return Jsoner.Convert(fullUser);

        }

        [HttpGet("remove-sysadmin")]
        public async Task<string> GetRemoveDemoUser()
        {
            var userNameToDelete = "stenvala";
            await UserLogic.DeleteUserByUserName(userNameToDelete);
            return "OK";
        }        
    }
}
